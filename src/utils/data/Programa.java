/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.data;

/**
 *
 * @author eryalus
 */
public class Programa {

    private String name = null, vendor = null, version = null, description = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Name: " + name + " - Version: " + version;
    }

    public String toSocket() {
        return name + "///" + version + "///" + vendor + "///" + description;
    }

    public boolean fromSocket(String text) {
        String[] partes = text.split("///");
        if (partes.length == 4) {
            this.name = partes[0];
            this.version = partes[1];
            this.vendor = partes[2];
            this.description = partes[3];
            return true;
        } else {
            return false;
        }
    }
}
