/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.cipher;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class DH {

    int bitLength = 512;
    int certainty = 20;// probabilistic prime generator 1-2^-certainty => practically 'almost sure'
    private static final SecureRandom rnd = new SecureRandom();
    //Bob
    public static int g = 3;
    public static int p = 7;
    private final SocketWriter writer;
    private final SocketReader reader;

    public DH(SocketWriter writer, SocketReader reader) {
        this.writer = writer;
        this.reader = reader;
    }

    public BigInteger handShake_Alice() throws IOException {
        Random randomGenerator = new Random();
        BigInteger generatorValue, primeValue, publicA, publicB, secretA, sharedKeyA;
        primeValue = findPrime();// BigInteger.valueOf((long)g);
        writer.writeString(primeValue.toString());
        writer.flush();
        generatorValue = findPrimeRoot(primeValue);//BigInteger.valueOf((long)p);
        secretA = new BigInteger(bitLength - 2, randomGenerator);
        publicA = generatorValue.modPow(secretA, primeValue);
        writer.writeString(publicA.toString());
        writer.flush();
        publicB = new BigInteger(reader.readString());
        sharedKeyA = publicB.modPow(secretA, primeValue);// should always be same as:
        return sharedKeyA;
    }

    public BigInteger handShake_Bob() throws IOException {
        Random randomGenerator = new Random();
        BigInteger generatorValue, primeValue, publicA, publicB, secretB, sharedKeyB;

        primeValue = new BigInteger(reader.readString());
        generatorValue = findPrimeRoot(primeValue);//BigInteger.valueOf((long)p);
        secretB = new BigInteger(bitLength - 2, randomGenerator);
        publicB = generatorValue.modPow(secretB, primeValue);
        writer.writeString(publicB.toString());
        writer.flush();
        publicA = new BigInteger(reader.readString());
        sharedKeyB = publicA.modPow(secretB, primeValue);
        return sharedKeyB;
    }

    public static boolean miller_rabin_pass(BigInteger a, BigInteger n) {
        BigInteger n_minus_one = n.subtract(BigInteger.ONE);
        BigInteger d = n_minus_one;
        int s = d.getLowestSetBit();
        d = d.shiftRight(s);
        BigInteger a_to_power = a.modPow(d, n);
        if (a_to_power.equals(BigInteger.ONE)) {
            return true;
        }
        for (int i = 0; i < s - 1; i++) {
            if (a_to_power.equals(n_minus_one)) {
                return true;
            }
            a_to_power = a_to_power.multiply(a_to_power).mod(n);
        }
        if (a_to_power.equals(n_minus_one)) {
            return true;
        }
        return false;
    }

    public static boolean miller_rabin(BigInteger n) {
        for (int repeat = 0; repeat < 20; repeat++) {
            BigInteger a;
            do {
                a = new BigInteger(n.bitLength(), rnd);
            } while (a.equals(BigInteger.ZERO));
            if (!miller_rabin_pass(a, n)) {
                return false;
            }
        }
        return true;
    }

    public boolean isPrime(BigInteger r) {
        return miller_rabin(r);
        // return BN_is_prime_fasttest_ex(r,bitLength)==1;
    }

    public List<BigInteger> primeFactors(BigInteger number) {
        BigInteger n = number;
        BigInteger i = BigInteger.valueOf(2);
        BigInteger limit = BigInteger.valueOf(10000);// speed hack! -> consequences ???
        List<BigInteger> factors = new ArrayList<BigInteger>();
        while (!n.equals(BigInteger.ONE)) {
            while (n.mod(i).equals(BigInteger.ZERO)) {
                factors.add(i);
                n = n.divide(i);
                // System.out.println(i);
                // System.out.println(n);
                if (isPrime(n)) {
                    factors.add(n);// yes?
                    return factors;
                }
            }
            i = i.add(BigInteger.ONE);
            if (i.equals(limit)) {
                return factors;// hack! -> consequences ???
            }		// System.out.print(i+"    \r");
        }
        System.out.println(factors);
        return factors;
    }

    boolean isPrimeRoot(BigInteger g, BigInteger p) {
        BigInteger totient = p.subtract(BigInteger.ONE); //p-1 for primes;// factor.phi(p);
        List<BigInteger> factors = primeFactors(totient);
        int i = 0;
        int j = factors.size();
        for (; i < j; i++) {
            BigInteger factor = factors.get(i);//elementAt
            BigInteger t = totient.divide(factor);
            if (g.modPow(t, p).equals(BigInteger.ONE)) {
                return false;
            }
        }
        return true;
    }

    String download(String address) {
        String txt = "";
        URLConnection conn = null;
        InputStream in = null;
        try {
            URL url = new URL(address);
            conn = url.openConnection();
            conn.setReadTimeout(10000);//10 secs
            in = conn.getInputStream();
            byte[] buffer = new byte[1024];
            int numRead;
            String encoding = "UTF-8";
            while ((numRead = in.read(buffer)) != -1) {
                txt += new String(buffer, 0, numRead, encoding);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return txt;
    }

    void compareWolfram(BigInteger p) {
        // String g= download("http://www.wolframalpha.com/input/?i=primitive+root+"+p);
        String url = "http://api.wolframalpha.com/v2/query?appid=&input=primitive+root+" + p;
        System.out.println(url);
        String g = download(url);;
        String[] vals = g.split(".plaintext>");
        if (vals.length < 3) {
            System.out.println(g);
        } else {
            System.out.println("wolframalpha generatorValue " + vals[3]);
        }
    }

    BigInteger findPrimeRoot(BigInteger p) {
        int start = 2001;// first best probably precalculated by NSA?
        // preferably  3, 17 and 65537
        if (start == 2) {
            compareWolfram(p);
        }

        for (int i = start; i < 100000000; i++) {
            if (isPrimeRoot(BigInteger.valueOf(i), p)) {
                return BigInteger.valueOf(i);
            }
        }
        // if(isPrimeRoot(i,p))return BigInteger.valueOf(i);
        return BigInteger.valueOf(0);
    }

    BigInteger findPrime() {
        Random rnd = new Random();
        BigInteger p = BigInteger.ZERO;
        // while(!isPrime(p))
        p = new BigInteger(bitLength, certainty, rnd);// sufficiently NSA SAFE?!!
        return p;

        // BigInteger r;
        // BigInteger r2= BN_generate_prime(r,512);
        //  System.out.println("isPrime(i)? "+r+" "+r2);
        // return r;
    }

}
