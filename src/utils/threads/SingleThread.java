/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.threads;

/**
 *
 * @author eryalus
 */
public class SingleThread extends Thread {

    private Thread hilo = null;
    protected String NOMBRE_HILO = "";

    /**
     *
     */
    @Override
    public void start() {
        if (hilo == null) {
            hilo = new Thread(this, NOMBRE_HILO);
            hilo.start();
        }
    }
}
