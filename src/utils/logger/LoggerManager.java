/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.logger;

import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class LoggerManager {

    private HashMap<Long, Logger> map = new HashMap<>();

    public synchronized Logger put(Long ref, Logger log) {
        return map.put(ref, log);
    }

    public synchronized void write(String txt, Long ref) {
        map.get(ref).write(txt);
    }

    public synchronized void remove(Long ref) {
        map.remove(ref);
    }
}
