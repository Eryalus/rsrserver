/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;

/**
 *
 * @author eryalus
 */
public class Logger {

    private final String path;
    private Long DEFAULT_REF = 0L;
    private BufferedWriter writer;

    public Logger(String logger_path) {
        path = logger_path;
    }

    public Logger(String logger_path, Long default_ref) {
        path = logger_path;
        DEFAULT_REF = default_ref;
    }

    private BufferedWriter getWriter() throws IOException {
        if (!new File(path).exists()) {
            new File(path).getParentFile().mkdirs();
            return new BufferedWriter(new FileWriter(path));
        }
        return new BufferedWriter(new FileWriter(path, true));
    }

    public void write(String text) {
        write(text, DEFAULT_REF);
    }

    public synchronized void write(String text, Long reference) {
        try {
            writer = getWriter();
            writer.write(reference + ": " + text + "\n");
            writer.close();
        } catch (IOException ex) {
            System.out.println("LoggerError: " + reference + ": " + text);
        }
    }
}
