/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.updater;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import server.RSRServer;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class Updater {

    protected Socket SOCKET;
    protected SocketWriter writer;
    protected SocketReader reader;
    protected String aula = null, numero = null;
    protected final Long logger_key;
    private static final String CHECK_MY_VERSION = "check_version";
    private static final String GET_LATEST = "get_latest";
    private static final String UPDATE_VERSION = "update_version";
    private final static String[] SUPPORTED_VERS = new String[]{"2.4","3.0"};

    public Updater(Socket SOCKET, SocketWriter writer, SocketReader reader, String aula, String numero, Long logger_key) {
        this.SOCKET = SOCKET;
        this.writer = writer;
        this.reader = reader;
        this.aula = aula;
        this.numero = numero;
        this.logger_key = logger_key;
    }

    protected void Log(String s) {
        RSRServer.writeLog(s, logger_key);
    }

    private boolean validate_vers(String vers) {
        for (int i = 0; i < SUPPORTED_VERS.length; i++) {
            if (SUPPORTED_VERS[i].equals(vers)) {
                return true;
            }
        }
        return false;
    }

    private boolean is_latests_vers(String vers) {
        return vers.equals(SUPPORTED_VERS[SUPPORTED_VERS.length - 1]);
    }

    private void check_version() throws IOException {
        String vers = reader.readString();
        RSRServer.writeLog("La versión enviada es la " + vers, logger_key);
        String msg = "";
        if (is_latests_vers(vers)) {
            RSRServer.writeLog("Es la más nueva", logger_key);
            writer.writeInt32(0);
            msg = "Ya tiene la última versión " + vers;
        } else if (validate_vers(vers)) {
            RSRServer.writeLog("Soportada", logger_key);
            writer.writeInt32(1);
            msg = "Hay actualizaciones disponibles";
        } else {
            RSRServer.writeLog("No soportada", logger_key);
            writer.writeInt32(-1);
            msg = "Su versión ya no está soportada por el servidor";
        }
        writer.writeString(msg);
        writer.flush();
    }

    private void update_version() throws IOException {
        File zip = new File("update.zip");
        File bat = new File("update.bat");
        if (zip.exists() && bat.exists()) {
            writer.writeLong(0);
            writer.flush();
            writer.writeFile(bat);
            writer.writeFile(zip);
            writer.flush();
        } else {
            writer.writeLong(-2);
            writer.writeString("Falatan ficheros. Contacte con el administrador del sistema.");
            writer.flush();
        }
    }

    public void run() throws IOException {
        String op = reader.readString();
        switch (op) {
            case CHECK_MY_VERSION:
                writer.writeLong(0);
                writer.flush();
                check_version();
                break;
            case GET_LATEST:
                writer.writeLong(0);
                writer.writeString("La última versión es " + SUPPORTED_VERS[SUPPORTED_VERS.length - 1]);
                writer.flush();
                break;
            case UPDATE_VERSION:
                update_version();
                break;
            default:
                writer.writeLong(-1);
                writer.writeString("Opción desconocida");
                writer.flush();
                break;
        }
    }

}
