/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import static server.FilesReadWrite.*;
import utils.database.ReguladorConexion;
import utils.logger.LoggerManager;
import utils.threads.SingleThread;
import utils.time.MyOwnCalendar;

/**
 *
 * @author eryalus
 */
public class RSRServer extends SingleThread {

    public static String PASS = "Faraday";
    public static ReguladorConexion regulador;
    private static final Integer TIEMPO_REFRESCO_CONEXION_MIN = 60;

    public static final Integer PORT = 10524;
    private String user, pass = "";
    private static LoggerManager MANAGER = new LoggerManager();

    private void showHelp() {
        System.out.println("Use:");
        System.out.println("[user] [password]");
    }

    public RSRServer(String[] args) {
        super.NOMBRE_HILO = "Hilo Principal";
        Scanner scan = new Scanner(System.in);
        try {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("help")) {
                    showHelp();
                    return;
                } else {
                    user = args[0];
                }
            } else if (args.length == 2) {
                user = args[0];
                pass = args[1];
            } else {
                boolean acaba = false, anon = false;
                while (!acaba) {
                    System.out.println("Es anónimo (s/n)?");
                    String op = scan.next();
                    switch (op) {
                        case "s":
                        case "S":
                            anon = true;
                            acaba = true;
                            break;
                        case "n":
                        case "N":
                            acaba = true;
                        default:
                            break;

                    }
                }
                if (!anon) {
                    System.out.println("Introduzca el nombre de usuario de la base de datos:");
                    user = scan.next();
                    acaba=false;
                    while (!acaba) {
                        System.out.println("Usa contraseña? (s/n):");
                        String op = scan.next();
                        switch (op) {
                            case "s":
                            case "S":
                                System.out.println("Introduzca la contraseña:");
                                pass = scan.next();
                                acaba = true;
                                break;
                            case "n":
                            case "N":
                                acaba = true;
                            default:
                                break;

                        }
                    }
                }
            }
            regulador = new ReguladorConexion(TIEMPO_REFRESCO_CONEXION_MIN * 60 * 1000, user, pass, "");
            regulador.start();
            if (!loadPass()) {
                if (pass != null) {
                    System.out.println("No se ha podido cargar la contraseña, se usará por defecto: " + PASS);
                    writePass(PASS);
                } else {
                    System.out.println("No se ha podido cargar la contraseña del fichero " + PASS_PATH);
                    System.exit(0);
                }
            }
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(RSRServer.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }

    }

    public static void writeLog(String txt, Long key) {
        MANAGER.write(txt, key);
    }

    public static void removeLog(Long key) {
        MANAGER.remove(key);
    }
    private static final String LOGGER_BASE_PATH = "logs/";

    /**
     *
     */
    @Override
    public void run() {
        try {
            Long ref = 0L;
            Long key = 0L;
            ServerSocket socket = new ServerSocket(PORT);
            new Comandos().start();
            while (true) {
                try {
                    Socket soc = socket.accept();
                    File logger_file = new File(LOGGER_BASE_PATH + new MyOwnCalendar().getDate(MyOwnCalendar.SPANISH).replace("/", "-") + ".log");
                    if (!logger_file.exists()) {
                        logger_file.getParentFile().mkdirs();
                        ref = 0L;
                        logger_file.createNewFile();
                    }
                    utils.logger.Logger log = new utils.logger.Logger(logger_file.getAbsolutePath(), ref++);
                    MANAGER.put(key++, log);
                    new AtencionSocket(soc, key - 1).start();
                } catch (IOException ex) {
                    Logger.getLogger(RSRServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(RSRServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
