/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import server.updater.Updater;
import server.versions.Version_1;
import server.versions.Version_Sup;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;
import utils.threads.SingleThread;
import utils.time.MyOwnCalendar;

/**
 *
 * @author eryalus
 */
public class AtencionSocket extends SingleThread {

    private final Socket soc;
    private final SocketReader reader;
    private final SocketWriter writer;
    private final Long ref;
    protected static final String basePath = "temp/";
    private String aula = null, numero = null;
    private final static String[] SUPPORTED_VERS = new String[]{"1.0", "1.1", "2.0", "2.1", "2.2", "2.3", "2.4"};
    private final Long logger_key;

    public AtencionSocket(Socket socket, Long logger_key) throws IOException {
        RSRServer.writeLog("Conexión entrante " + new MyOwnCalendar().toString() + ": " + socket.getInetAddress().getHostName() + ":" + socket.getPort(), logger_key);
        super.NOMBRE_HILO = "Hilo de atención al socket";
        this.soc = socket;
        reader = new SocketReader(soc);
        writer = new SocketWriter(soc);
        ref = new MyOwnCalendar().getTimeInMillis();
        this.logger_key = logger_key;
        //System.out.println("New connection");
    }

    private void delete() {
        new File(basePath + aula + "-" + numero + "-datos-soft-hard.txt").delete();
    }

    private boolean validate_vers(String vers) {
        for (int i = 0; i < SUPPORTED_VERS.length; i++) {
            if (SUPPORTED_VERS[i].equals(vers)) {
                return true;
            }
        }
        return false;
    }

    private boolean is_latests_vers(String vers) {
        return vers.equals(SUPPORTED_VERS[SUPPORTED_VERS.length - 1]);
    }

    private void check_vers() throws IOException {
        String vers = reader.readString();
        RSRServer.writeLog("La versión enviada es la " + vers, logger_key);
        String msg = "";
        if (is_latests_vers(vers)) {
            RSRServer.writeLog("Es la más nueva", logger_key);
            writer.writeInt32(0);
            msg = "Ya tiene la última versión " + vers;
        } else if (validate_vers(vers)) {
            RSRServer.writeLog("Soportada", logger_key);
            writer.writeInt32(1);
            msg = "Hay actualizaciones disponibles";
        } else {
            RSRServer.writeLog("No soportada", logger_key);
            writer.writeInt32(-1);
            msg = "Su versión ya no está soportada por el servidor";
        }
        writer.writeString(msg);
        writer.flush();
    }

    private void send_new_vers() throws IOException {
        File orig = new File("latest.jar");
        if (orig.exists()) {
            RSRServer.writeLog("Nueva versión enviada", logger_key);
            writer.writeInt32(0);
            writer.writeFile(orig);
        } else {
            RSRServer.writeLog("Falta el fichero de la nueva versión", logger_key);
            writer.writeInt32(1);
        }
        writer.flush();
        soc.close();
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            Integer op = reader.readInt32();
            RSRServer.writeLog("Ha recibido la operación: " + op, logger_key);
            switch (op) {
                case 0:
                    RSRServer.writeLog("Comprobar versión", logger_key);
                    check_vers();
                    break;
                case 1:
                    RSRServer.writeLog("Solicita envío de la nueva versión", logger_key);
                    send_new_vers();
                    break;
                case 2:
                    RSRServer.writeLog("Envía datos, versión 1", logger_key);
                    Version_1 vers = new Version_1(soc, writer, reader, aula, numero, logger_key);
                    vers.response();
                    aula = vers.getAula();
                    numero = vers.getNumero();
                    break;
                case 3:
                    Version_Sup vers_2 = new Version_Sup(soc, writer, reader, aula, numero, logger_key);
                    vers_2.response();
                    aula = vers_2.getAula();
                    numero = vers_2.getNumero();
                    break;
                case 4:
                    RSRServer.writeLog("Conexión updater v2", logger_key);
                    Updater updater = new Updater(soc, writer, reader, aula, numero, logger_key);
                    updater.run();
                default:
                    break;
            }
            if (aula != null && numero != null) {
                delete();
            }
            soc.close();

        } catch (Exception ex) {
            String error = "";
            for (StackTraceElement e : ex.getStackTrace()) {
                error += e.toString() + "\n";
            }
            RSRServer.writeLog("ERROR:", logger_key);
            RSRServer.writeLog(ex.getMessage(), logger_key);
            RSRServer.writeLog(error, logger_key);

        } finally {
            RSRServer.removeLog(logger_key);
        }
    }

}
