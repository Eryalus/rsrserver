/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.util.Scanner;
import utils.threads.SingleThread;

/**
 *
 * @author eryalus
 */
public class Comandos extends SingleThread {

    public Comandos() {
        super.NOMBRE_HILO = "Hilo de comandos";
    }

    private void help() {
        System.out.println("Ayuda:");
        System.out.println();
        System.out.println("passwd - Cambiar la contraseña del servicio.");
        System.out.println("info - Muestra información del sistema.");
        System.out.println("exit - Sale del programa.");
        System.out.println("help - muestra un mensaje de ayuda.");
    }

    private void exit() {
        System.out.println("Saliendo...");
        System.exit(0);
    }

    private void info() {
        String ip = "Desconocida";
        try {
            ip = utils.networking.IPv4.getLansIP().getHostAddress();
        } catch (IOException ex) {
        }
        System.out.println("IP: " + ip);
        System.out.println("Puerto: " + RSRServer.PORT);
        System.out.println("Password: " + RSRServer.PASS);
    }

    private final static String valid_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private boolean validarPass(String pass) {
        for (int i = 0; i < pass.length(); i++) {
            if (!valid_chars.contains("" + pass.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private void passwd(Scanner scan) {
        System.out.println("La contraseña actual es: " + RSRServer.PASS);

        while (true) {
            System.out.print("¿Desea cambiarla? (s/n) ");
            String op = scan.next();
            switch (op) {
                case "s":
                case "S":
                    while (true) {
                        System.out.print("Introduzca la nueva contraseña: ");
                        String pass;
                        pass = scan.next();
                        if (validarPass(pass)) {
                            if (FilesReadWrite.writePass(pass)) {
                                if (FilesReadWrite.loadPass()) {
                                    System.out.println("Se ha cambiado y refrescado la contraseña.");
                                    return;
                                } else {
                                    System.out.println("Se ha podido guardar la contraseña pero no refrescarla.");
                                    return;
                                }
                            } else {
                                System.out.println("No se ha podido guardar la nueva contraseña.");
                                return;
                            }
                        } else {
                            System.out.println("La contraseña no es válida, solo puede contener los siguientes caracteres: " + valid_chars);
                        }
                    }
                case "n":
                case "N":
                    System.out.println("La contraseña no ha sido cambiada.");
                    return;
                default:
                    System.out.println("La opción no es válida.");
            }
        }
    }

    /**
     *
     */
    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.print(">");
            String line = scan.next();
            switch (line.toLowerCase()) {
                case "passwd":
                    passwd(scan);
                    break;
                case "salir":
                case "exit":
                case "quit":
                    exit();
                    break;
                case "ayuda":
                case "help":
                    help();
                    break;
                case "info":
                    info();
                    break;
                default:
                    System.out.println("Ingrese un comando válido. Use \"help\" para ver la ayuda.");
                    break;
            }
        }
    }
}
