/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.versions;

import java.net.Socket;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public abstract class Version {

    protected Socket SOCKET;
    protected SocketWriter writer;
    protected SocketReader reader;
    protected String aula = null, numero = null;

    public Version(Socket SOCKET, SocketWriter writer, SocketReader reader, String aula, String numero, Long logger_key) {
        this.SOCKET = SOCKET;
        this.writer = writer;
        this.reader = reader;
        this.aula = aula;
        this.numero = numero;
        this.logger_key = logger_key;
    }
    protected static final String basePath = "temp/";
    protected final Long logger_key;

    public abstract void response();

    public String getAula() {
        return aula;
    }

    public String getNumero() {
        return numero;
    }
}
