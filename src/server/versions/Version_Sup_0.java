/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.versions;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.RSRServer;
import static server.versions.Version.basePath;
import utils.data.Disk;
import utils.data.Programa;
import utils.database.Basics;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class Version_Sup_0 extends Version {

    public Version_Sup_0(Socket SOCKET, SocketWriter writer, SocketReader reader, String aula, String numero, Long logger_key) {
        super(SOCKET, writer, reader, aula, numero, logger_key);
    }

    @Override
    public void response() {
        try {
            while (reader.readLong() == 1) {
                String sended_pass = reader.readString();
                if (sended_pass.equals(RSRServer.PASS)) {
                    RSRServer.writeLog("Contraseña correcta", logger_key);
                    writer.writeLong(1);
                    break;
                } else {
                    RSRServer.writeLog("Contraseña incorrecta", logger_key);
                    writer.writeLong(0);
                }
                writer.flush();
            }
            aula = reader.readString();
            numero = reader.readString();
            RSRServer.writeLog("Aula: " + aula + " Número: " + numero, logger_key);
            if (Basics.getIDEquipo(aula, numero) != -1) {
                writer.writeLong(1);
            } else {
                writer.writeLong(0);
            }
            writer.flush();
            if (reader.readLong() != 0) {
                return;
            }
            new File(basePath).mkdirs();
            boolean error = false;
            if (error) {
                writer.writeLong(1);
                writer.writeString("Ha ocurrido un error durante el envío. Vuelva a intentarlo");
                return;
            }
            String path = basePath + aula + "-" + numero + "-datos-soft-hard.txt";
            ArrayList<Programa> programs = new ArrayList<>();
            ArrayList<Disk> discos = new ArrayList<>();
            ArrayList<String> partitions = new ArrayList<>();
            String IP = null, nombre_procesador = null, mother = null, videocard = null, mac = null;
            Integer cores = null;
            Long ramsize = null;
            while (reader.readLong() == 1) {
                Programa p = new Programa();
                if (p.fromSocket(reader.readString()) == true) {
                    programs.add(p);
                }
            }
            nombre_procesador = reader.readString();
            cores = Integer.parseInt(reader.readString());
            mother = reader.readString();
            videocard = reader.readString();
            IP = reader.readString();
            mac = reader.readString();
            ramsize = reader.readLong();
            while (reader.readLong() == 1) {
                Disk d = new Disk();
                if (d.fromSocket(reader.readString()) == true) {
                    discos.add(d);
                }
            }
            while (reader.readLong() == 1) {
                partitions.add(reader.readString());
            }
            RSRServer.writeLog("Importando nuevos datos a la base de datos...", logger_key);
            try {
                Basics.delEquipo(aula, numero);
                ArrayList<Integer> diskids = new ArrayList<>();
                for (Disk disk : discos) {
                    if (Basics.getIDDisk(disk.getName(), disk.getSize()) == -1) {
                        Basics.addDisk(disk.getName(), disk.getSize());
                    }
                    diskids.add(Basics.getIDDisk(disk.getName(), disk.getSize()));
                }
                Basics.addEquipo(aula, numero, diskids, nombre_procesador, cores, mother, videocard, mac, ramsize, IP);
                for (Programa p : programs) {
                    Basics.addProgram(aula, numero, p);
                }
                for (String partition : partitions) {
                    String[] pts = partition.split(",");
                    if (pts.length == 4) {
                        String name, free, total;
                        name = pts[2];
                        try {
                            
                            free = "" + Long.parseLong(pts[1]);
                            total = "" + Long.parseLong(pts[3]);
                            Basics.addPartition(aula, numero, name, total, free);
                        } catch (NumberFormatException ex) {

                        }
                    }
                }
            } catch (SQLException ex) {
                writer.writeLong(1);
                writer.writeString(ex.getMessage());
                String error_ex = "";
                for (StackTraceElement e : ex.getStackTrace()) {
                    error_ex += e.toString() + "\n";
                }
                RSRServer.writeLog("ERROR:", logger_key);
                RSRServer.writeLog(ex.getMessage(), logger_key);
                RSRServer.writeLog(error_ex, logger_key);
                return;
            }
            writer.writeLong(0);
            RSRServer.writeLog("Conexión finalizada", logger_key);
        } catch (IOException ex) {
            Logger.getLogger(Version_Sup_0.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
