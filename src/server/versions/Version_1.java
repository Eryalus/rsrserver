/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.versions;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import server.RSRServer;
import utils.data.Disk;
import utils.data.Programa;
import utils.database.Basics;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class Version_1 extends Version {

    public Version_1(Socket SOCKET, SocketWriter writer, SocketReader reader, String aula, String numero, Long logger_key) {
        super(SOCKET, writer, reader, aula, numero, logger_key);
    }

    @Override
    public void response() {
        RSRServer.writeLog("Leyendo datos entrantes...", logger_key);
        try {
            while (reader.readLong() == 1) {
                String sended_pass = reader.readString();
                if (sended_pass.equals(RSRServer.PASS)) {
                    RSRServer.writeLog("Contraseña correcta", logger_key);
                    writer.writeLong(1);
                    break;
                } else {
                    RSRServer.writeLog("Contraseña incorrecta", logger_key);
                    writer.writeLong(0);
                }
                writer.flush();
            }
            aula = reader.readString();
            numero = reader.readString();
            RSRServer.writeLog("Aula: " + aula + " Número: " + numero, logger_key);
            if (Basics.getIDEquipo(aula, numero) != -1) {
                writer.writeLong(1);
            } else {
                writer.writeLong(0);
            }
            writer.flush();
            if (reader.readLong() != 0) {
                return;
            }
            new File(basePath).mkdirs();
            boolean error = false;
            if (error) {
                writer.writeLong(1);
                writer.writeString("Ha ocurrido un error durante el envío. Vuelva a intentarlo");
                return;
            }
            String path = basePath + aula + "-" + numero + "-datos-soft-hard.txt";
            ArrayList<Programa> programs = new ArrayList<>();
            ArrayList<Disk> discos = new ArrayList<>();
            String IP = null, nombre_procesador = null, mother = null, videocard = null, mac = null;
            Integer cores = null;
            Long ramsize = null;
            while (reader.readLong() == 1) {
                Programa p = new Programa();
                if (p.fromSocket(reader.readString()) == true) {
                    programs.add(p);
                }
            }
            nombre_procesador = reader.readString();
            cores = Integer.parseInt(reader.readString());
            mother = reader.readString();
            videocard = reader.readString();
            IP = reader.readString();
            mac = reader.readString();
            ramsize = reader.readLong();
            while (reader.readLong() == 1) {
                Disk d = new Disk();
                if (d.fromSocket(reader.readString()) == true) {
                    discos.add(d);
                }
            }
            RSRServer.writeLog("Importando nuevos datos a la base de datos...", logger_key);
            try {
                Basics.delEquipo(aula, numero);
                ArrayList<Integer> diskids = new ArrayList<>();
                for (Disk disk : discos) {
                    if (Basics.getIDDisk(disk.getName(), disk.getSize()) == -1) {
                        Basics.addDisk(disk.getName(), disk.getSize());
                    }
                    diskids.add(Basics.getIDDisk(disk.getName(), disk.getSize()));
                }
                Basics.addEquipo(aula, numero, diskids, nombre_procesador, cores, mother, videocard, mac, ramsize, IP);
                for (Programa p : programs) {
                    Basics.addProgram(aula, numero, p);
                }

            } catch (SQLException ex) {
                writer.writeLong(1);
                writer.writeString(ex.getMessage());
                String error_ex = "";
                for (StackTraceElement e : ex.getStackTrace()) {
                    error_ex += e.toString() + "\n";
                }
                RSRServer.writeLog("ERROR:", logger_key);
                RSRServer.writeLog(ex.getMessage(), logger_key);
                RSRServer.writeLog(error_ex, logger_key);
                return;
            }
            writer.writeLong(0);
            RSRServer.writeLog("Conexión finalizada", logger_key);
        } catch (IOException ex) {
            String error = "";
            for (StackTraceElement e : ex.getStackTrace()) {
                error += e.toString() + "\n";
            }
            RSRServer.writeLog("ERROR:", logger_key);
            RSRServer.writeLog(ex.getMessage(), logger_key);
            RSRServer.writeLog(error, logger_key);
        }
    }

}
