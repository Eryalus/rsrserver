/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.versions;

import java.io.IOException;
import java.net.Socket;
import server.RSRServer;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class Version_Sup extends Version {

    public Version_Sup(Socket SOCKET, SocketWriter writer, SocketReader reader, String aula, String numero, Long logger_key) {
        super(SOCKET, writer, reader, aula, numero, logger_key);
    }

    @Override
    public void response() {
        try {
            String version = reader.readString();
            redirect(version);
        } catch (IOException ex) {
            String error = "";
            for (StackTraceElement e : ex.getStackTrace()) {
                error += e.toString() + "\n";
            }
            RSRServer.writeLog("ERROR:", logger_key);
            RSRServer.writeLog(ex.getMessage(), logger_key);
            RSRServer.writeLog(error, logger_key);
        }
    }

    private void redirect(String version) throws IOException {
        Version vers;
        switch (version) {
            case "2.0":
            case "2.1":
                RSRServer.writeLog("Leyendo datos entrantes... Version: " + version, logger_key);
                writer.writeLong(0L);
                vers = new Version_Sup_0(SOCKET, writer, reader, aula, numero, logger_key);
                vers.response();
                aula = vers.getAula();
                numero = vers.getNumero();
                break;
            case "2.2":
            case "2.3":
            case "2.4":
                RSRServer.writeLog("Leyendo datos entrantes... Version: " + version, logger_key);
                writer.writeLong(0L);
                vers = new Version_Sup_2(SOCKET, writer, reader, aula, numero, logger_key);
                vers.response();
                aula = vers.getAula();
                numero = vers.getNumero();
                break;
            case "3.0":
                RSRServer.writeLog("Leyendo datos entrantes... Version: " + version, logger_key);
                writer.writeLong(0L);
                vers = new Version_Sup_3(SOCKET, writer, reader, aula, numero, logger_key);
                vers.response();
                aula = vers.getAula();
                numero = vers.getNumero();
                break;
            default:
                RSRServer.writeLog("Versión no soportada: " + version, logger_key);
                writer.writeLong(-1L);
                writer.writeString("Versión no soportada. Por favor actualice su aplicación");
                break;
        }
    }

}
