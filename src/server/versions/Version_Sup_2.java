/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.versions;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.RSRServer;
import static server.versions.Version.basePath;
import utils.data.Disk;
import utils.data.Programa;
import utils.database.Basics;
import utils.socket.SocketReader;
import utils.socket.SocketWriter;

/**
 *
 * @author ad_ri
 */
public class Version_Sup_2 extends Version {

    public Version_Sup_2(Socket SOCKET, SocketWriter writer, SocketReader reader, String aula, String numero, Long logger_key) {
        super(SOCKET, writer, reader, aula, numero, logger_key);
    }

    @Override
    public void response() {
        try {
            while (reader.readLong() == 1) {
                String sended_pass = reader.readString();
                if (sended_pass.equals(RSRServer.PASS)) {
                    RSRServer.writeLog("Contraseña correcta", logger_key);
                    writer.writeLong(1);
                    break;
                } else {
                    RSRServer.writeLog("Contraseña incorrecta", logger_key);
                    writer.writeLong(0);
                }
                writer.flush();
            }
            aula = reader.readString();
            numero = reader.readString();
            RSRServer.writeLog("Aula: " + aula + " Número: " + numero, logger_key);
            if (Basics.getIDEquipo(aula, numero) != -1) {
                writer.writeLong(1);
            } else {
                writer.writeLong(0);
            }
            writer.flush();
            if (reader.readLong() != 0) {
                return;
            }
            new File(basePath).mkdirs();
            boolean error = false;
            if (error) {
                writer.writeLong(1);
                writer.writeString("Ha ocurrido un error durante el envío. Vuelva a intentarlo");
                return;
            }
            String path = basePath + aula + "-" + numero + "-datos-soft-hard.txt";
            ArrayList<Programa> programs = new ArrayList<>();
            ArrayList<Disk> discos = new ArrayList<>();
            ArrayList<Partition> partitions = new ArrayList<>();
            String IP = null, nombre_procesador = null, mother = null, videocard = null, mac = null;
            Integer cores = null;
            Long ramsize = null;
            //read xml
            reader.readFile(path);

            try {
                //process xml file
                Data data = readDataFromXML(path);
                discos = data.getDiscos();
                programs = data.getPrograms();
                partitions = data.getPartitions();
                nombre_procesador = data.getNombre_procesador();
                cores = data.getCores();
                mother = data.getMother();
                videocard = data.getVideocard();
                mac = data.getMac();
                ramsize = data.getRamsize();
                IP = data.getIP();
                RSRServer.writeLog("Importando nuevos datos a la base de datos...", logger_key);
                Basics.delEquipo(aula, numero);
                ArrayList<Integer> diskids = new ArrayList<>();
                for (Disk disk : discos) {
                    if (Basics.getIDDisk(disk.getName(), disk.getSize()) == -1) {
                        Basics.addDisk(disk.getName(), disk.getSize());
                    }
                    diskids.add(Basics.getIDDisk(disk.getName(), disk.getSize()));
                }
                Basics.addEquipo(aula, numero, diskids, nombre_procesador, cores, mother, videocard, mac, ramsize, IP);
                for (Programa p : programs) {
                    Basics.addProgram(aula, numero, p);
                }
                for (Partition partition : partitions) {
                    Basics.addPartition(aula, numero, partition.getName(), "" + partition.getSize(), "" + partition.getFree_space());
                }
            } catch (Exception ex) {
                writer.writeLong(1);
                writer.writeString(ex.getMessage());
                String error_ex = "";
                for (StackTraceElement e : ex.getStackTrace()) {
                    error_ex += e.toString() + "\n";
                }
                RSRServer.writeLog("ERROR:", logger_key);
                RSRServer.writeLog(ex.getMessage(), logger_key);
                RSRServer.writeLog(error_ex, logger_key);
                return;
            }
            writer.writeLong(0);
            RSRServer.writeLog("Conexión finalizada", logger_key);
        } catch (IOException ex) {
            Logger.getLogger(Version_Sup_2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Data readDataFromXML(String xmlFile) throws NumberFormatException, ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        Data data = new Data();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new File(xmlFile));
        //read data
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "/computer/@class";
        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        aula = nodeList.item(0).getTextContent();
        expression = "/computer/@number";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        numero = nodeList.item(0).getTextContent();
        expression = "/computer/@ip";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setIP(nodeList.item(0).getTextContent());
        expression = "/computer/processor/name";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setNombre_procesador(nodeList.item(0).getTextContent());
        expression = "/computer/processor/cores";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setCores(Integer.parseInt(nodeList.item(0).getTextContent()));
        expression = "/computer/motherboard";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setMother(nodeList.item(0).getTextContent());
        expression = "/computer/mac";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setMac(nodeList.item(0).getTextContent());
        expression = "/computer/videocard";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setVideocard(nodeList.item(0).getTextContent());
        expression = "/computer/ramsize";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        data.setRamsize(Long.parseLong(nodeList.item(0).getTextContent()));
        ArrayList<Disk> disks = new ArrayList<>();
        expression = "/computer/physicaldisks/disk";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node disk_n = nodeList.item(index);
            Disk d = new Disk();
            NodeList n2 = disk_n.getChildNodes();
            for (int i = 0; i < n2.getLength(); i++) {
                if (n2.item(i).getNodeName().equals("name")) {
                    d.setName(n2.item(i).getChildNodes().item(0).getNodeValue());
                } else if (n2.item(i).getNodeName().equals("capacity")) {
                    d.setSize(Long.parseLong(n2.item(i).getChildNodes().item(0).getNodeValue()));
                }
            }
            disks.add(d);
        }
        data.setDiscos(disks);
        ArrayList<Partition> partitions = new ArrayList<>();
        expression = "/computer/partitions/partition";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node disk_n = nodeList.item(index);
            Partition p = new Partition();
            NodeList n2 = disk_n.getChildNodes();
            for (int i = 0; i < n2.getLength(); i++) {
                if (n2.item(i).getNodeName().equals("name")) {
                    p.setName(n2.item(i).getChildNodes().item(0).getNodeValue());
                } else if (n2.item(i).getNodeName().equals("size")) {
                    p.setSize(Long.parseLong(n2.item(i).getChildNodes().item(0).getNodeValue()));
                } else if (n2.item(i).getNodeName().equals("freespace")) {
                    p.setFree_space(Long.parseLong(n2.item(i).getChildNodes().item(0).getNodeValue()));
                }
            }
            partitions.add(p);
        }
        data.setPartitions(partitions);
        ArrayList<Programa> programs = new ArrayList<>();
        expression = "/computer/programs/program";
        nodeList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node disk_n = nodeList.item(index);
            Programa p = new Programa();
            NodeList n2 = disk_n.getChildNodes();
            for (int i = 0; i < n2.getLength(); i++) {
                if (n2.item(i).getNodeName().equals("name")) {
                    p.setName(n2.item(i).getChildNodes().item(0).getNodeValue());
                } else if (n2.item(i).getNodeName().equals("vendor")) {
                    p.setVendor(n2.item(i).getChildNodes().item(0).getNodeValue());
                } else if (n2.item(i).getNodeName().equals("version")) {
                    p.setVersion(n2.item(i).getChildNodes().item(0).getNodeValue());
                } else if (n2.item(i).getNodeName().equals("description")) {
                    p.setDescription(n2.item(i).getChildNodes().item(0).getNodeValue());
                }
            }
            programs.add(p);
        }
        data.setPrograms(programs);
        return data;
    }

    private class Partition {

        String name;
        Long free_space, size;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getFree_space() {
            return free_space;
        }

        public void setFree_space(Long free_space) {
            this.free_space = free_space;
        }

        public Long getSize() {
            return size;
        }

        public void setSize(Long size) {
            this.size = size;
        }
    }

    private class Data {

        ArrayList<Programa> programs = new ArrayList<>();
        ArrayList<Disk> discos = new ArrayList<>();
        ArrayList<Partition> partitions = new ArrayList<>();
        String IP = null, nombre_procesador = null, mother = null, videocard = null, mac = null;
        Integer cores = null;
        Long ramsize = null;

        public ArrayList<Programa> getPrograms() {
            return programs;
        }

        public void setPrograms(ArrayList<Programa> programs) {
            this.programs = programs;
        }

        public ArrayList<Disk> getDiscos() {
            return discos;
        }

        public void setDiscos(ArrayList<Disk> discos) {
            this.discos = discos;
        }

        public ArrayList<Partition> getPartitions() {
            return partitions;
        }

        public void setPartitions(ArrayList<Partition> partitions) {
            this.partitions = partitions;
        }

        public String getIP() {
            return IP;
        }

        public void setIP(String IP) {
            this.IP = IP;
        }

        public String getNombre_procesador() {
            return nombre_procesador;
        }

        public void setNombre_procesador(String nombre_procesador) {
            this.nombre_procesador = nombre_procesador;
        }

        public String getMother() {
            return mother;
        }

        public void setMother(String mother) {
            this.mother = mother;
        }

        public String getVideocard() {
            return videocard;
        }

        public void setVideocard(String videocard) {
            this.videocard = videocard;
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }

        public Integer getCores() {
            return cores;
        }

        public void setCores(Integer cores) {
            this.cores = cores;
        }

        public Long getRamsize() {
            return ramsize;
        }

        public void setRamsize(Long ramsize) {
            this.ramsize = ramsize;
        }
    }

}
