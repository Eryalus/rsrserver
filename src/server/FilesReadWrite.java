/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static server.RSRServer.PASS;

/**
 *
 * @author eryalus
 */
public class FilesReadWrite {

    public static final String PASS_PATH = "data/password.txt";

    public static boolean loadPass() {
        BufferedReader br = null;
        new File(PASS_PATH.substring(0, PASS_PATH.lastIndexOf("/"))).mkdirs();
        File f = new File(PASS_PATH);
        try {
            br = new BufferedReader(new FileReader(f));
            PASS = br.readLine();
            br.close();
        } catch (FileNotFoundException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    public static boolean writePass(String pass) {
        BufferedWriter bw = null;
        new File(PASS_PATH.substring(0, PASS_PATH.lastIndexOf("/"))).mkdirs();
        File f = new File(PASS_PATH);
        try {
            bw = new BufferedWriter(new FileWriter(f));
            bw.write(pass);
        } catch (IOException ex) {
            return false;
        } finally {
            try {
                bw.close();
            } catch (IOException ex) {
                return false;
            }
        }
        return true;
    }
}
